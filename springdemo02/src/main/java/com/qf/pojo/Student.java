package com.qf.pojo;

/**
 * @author zhaojian
 */
public class Student {

    private Integer id;
    private String name;
    private String sex;

    public Student() {
    }

    public Student(Integer id, String name, String sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }


}
