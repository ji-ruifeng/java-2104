package com.qf.service;

import com.qf.pojo.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 11:14
 * description:
 */

public class OrderServiceImpl implements OrderService{
    @Override
    public List<Order> findOrderAll() {
        System.out.println("=====核心功能===查询订单所有数据====");
        return new ArrayList<Order>();
    }

    @Override
    public void insertOrder(Order order) {
        System.out.println("=====核心功能===添加订单数据====");
    }

    @Override
    public void updateOrder(Order order) {
        System.out.println("=====核心功能===修改订单数据====");
    }

    @Override
    public void deleteById(Integer id) {
        System.out.println("=====核心功能===删除订单数据====");
        int i = 1/0;

    }
}
