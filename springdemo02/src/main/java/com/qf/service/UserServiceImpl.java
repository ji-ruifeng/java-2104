package com.qf.service;

import com.qf.dao.UserDao;
import com.qf.pojo.User;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/28 14:55
 * description:
 */

public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        System.out.println("==========set===========");
        this.userDao = userDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public UserServiceImpl() {
        System.out.println("===========构造方法========");
    }

    @Override
    public List<User> getUserAll() {
        //UserDao userDao = new UserDaoTwoImpl();
        List<User> list = userDao.findUserAll();
        return list;
    }


    @PostConstruct //初始化
    public void init() {
        System.out.println("============init method executed=============");
    }

    @PreDestroy //销毁
    public void destroy() {
        System.out.println("===============destroy method executed=========");

    }
}