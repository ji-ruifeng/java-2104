package com.qf.service;

import com.qf.pojo.Order;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 11:14
 * description:
 */
public interface OrderService {
    public List<Order> findOrderAll();

    public void insertOrder(Order order);

    public void updateOrder(Order order);

    void deleteById(Integer id);


}
