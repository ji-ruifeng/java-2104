package com.qf.proxy;

import com.qf.service.FangDongService;
import com.qf.service.FangDongServiceImpl;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 11:12
 * description:中介代理类
 * 替房东租房子, 完成防止的维护等功能
 */

public class ZhongJieProxy implements FangDongService {

    private FangDongService fangDongService=new FangDongServiceImpl();
    @Override
    public void zuFang() {
        //添加辅助功能
        System.out.println("=====辅助功能=======发布租房信息=====");
        System.out.println("=====辅助功能=======中介带租客看房=====");

        //核心 = 原始业务类中的功能
        fangDongService.zuFang();

        //添加辅助功能
        System.out.println("======辅助功能=======中介替房东修防止===========");
        System.out.println("=======辅助功能====中介替房东承担法律风险========");
    }


    public static void main(String[] args) {
        ZhongJieProxy proxy = new ZhongJieProxy();
        proxy.zuFang();
    }
}
