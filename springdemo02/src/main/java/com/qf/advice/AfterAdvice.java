package com.qf.advice;

import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 11:26
 * description:  后置通知:
 * 先执行你的service业务方法
 * 在执行后置通知中添加辅助功能的方法
 */

public class AfterAdvice implements AfterReturningAdvice {
    @Override
    public void afterReturning(Object o, Method method, Object[] objects, Object o1) throws Throwable {
        System.out.println("=====辅助功能, 后置通知========打印执行日志==========");
    }
}
