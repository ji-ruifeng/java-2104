package com.qf.advice;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 11:27
 * description:  前置通知:
 * 先执行这个通知方法, 添加辅助功能
 * 再执行你的service中的业务方法
 */

public class BeforeAdvice implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] objects, Object o) throws Throwable {
        System.out.println("=======辅助功能, 前置通知=========打印传入方法的参数==========");
    }
}
