package com.qf.advice;

import org.springframework.aop.ThrowsAdvice;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 11:27
 * description: 异常通知:
 * 只有代码抛异常了才会执行这个异常通知中的方法
 */

public class ExceptionAdvice implements ThrowsAdvice {
    public void afterThrowing(Exception ex){
        System.out.println("异常通知: my throws 异常通知辅助功能");
    }
}
