package com.qf.advice;

import com.sun.corba.se.spi.ior.ObjectKey;
import org.aopalliance.intercept.Invocation;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 11:28
 * description: 环绕通知:
 * 在你的service业务方法执行前后, 都要执行这个环绕通知中添加的辅助功能方法
 */

public class RoundAdvice implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        System.out.println("=====辅助功能, 环绕通知==========开始事务=============");

        //运行service业务方法
        Object proceed= invocation.proceed();

        System.out.println("=====辅助功能, 环绕通知==========提交事务=============");
        return null;
    }
}
