package com.qf.test;

import com.qf.dao.UserDao;
import com.qf.pojo.Student;
import com.qf.pojo.User;
import com.qf.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/28 14:48
 * description:
 */

public class TestCreateBean {
    /**
     * 演示创建userDao实例化对象
     */
    @Test
    public void testCreateDao(){
        //1. 获取spring核心对象ApplicationContext
        String xml = "applicationContext.xml";
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xml);

        //2. 通过spring核心对象的getBean方法获取对象实例
        UserDao userDao = (UserDao) applicationContext.getBean("userDao");

        //3. 调用对象方法
        List<User> list = userDao.findUserAll();

        //4. 打印结果
        System.out.println("===========" + list);


    }

    /**
     * 演示创建UserService实例化对象
     */
    @Test
    public void testCreateService() {
        //1. 获取spring核心对象ApplicationContext
        String xml = "applicationContext.xml";
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xml);

        //2. 通过spring核心对象的getBean方法获取对象实例
        UserService userService = (UserService) applicationContext.getBean("userService");

        //3. 调用对象方法
        List<User> list = userService.getUserAll();

        //4. 打印结果
        System.out.println("===========" + list);

        //关闭spring对象工厂
        ((ClassPathXmlApplicationContext) applicationContext).close();
    }

    /**
     * 演示使用Set方法注入各种类型的数据
     */
    @Test
    public void testCreateUserBean() {
        //1. 获取spring核心对象ApplicationContext
        String xml = "applicationContext.xml";
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xml);

        User user = (User) applicationContext.getBean("user");
        System.out.println("=======" + user);
    }

    /**
     * 演示使用构造器, 或者叫做构造方法注入数据
     */
    @Test
    public void testCreateStudentBean() {
        //1. 获取spring核心对象ApplicationContext
        String xml = "applicationContext.xml";
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xml);

        Student student = (Student) applicationContext.getBean("student");
        System.out.println("=======" + student);
    }




}
