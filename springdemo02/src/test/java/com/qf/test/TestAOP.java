package com.qf.test;

import com.qf.pojo.Order;
import com.qf.service.OrderService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 11:43
 * description:
 */

public class TestAOP {
    /**
     * 测试前置通知
     */
    @Test
    public void testBefore() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");

        OrderService orderService = (OrderService) applicationContext.getBean("orderService");

        orderService.findOrderAll();
    }

    /**
     * 测试后置通知
     */
    @Test
    public void testAfter() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");

        OrderService orderService = (OrderService) applicationContext.getBean("orderService");

        orderService.insertOrder(new Order());
    }

    /**
     * 测试环绕通知
     */
    @Test
    public void testRound() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");

        OrderService orderService = (OrderService) applicationContext.getBean("orderService");

        orderService.updateOrder(new Order());
    }

    /**
     * 测试环绕通知
     */
    @Test
    public void testException() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");

        OrderService orderService = (OrderService) applicationContext.getBean("orderService");

        orderService.deleteById(1);
    }

}
