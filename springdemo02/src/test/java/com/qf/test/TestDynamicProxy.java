package com.qf.test;

import com.qf.service.FangDongService;
import com.qf.service.FangDongServiceImpl;

import org.junit.Test;
import org.springframework.cglib.proxy.Enhancer;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 11:43
 * description:
 */

public class TestDynamicProxy {
    /**
     * 测试jdk中的动态代理
     */
    @Test
    public void testJDK(){
        //声明代理目标
        FangDongService fangDongService=new FangDongServiceImpl();

        //使用jdk动态代理添加辅助功能
        InvocationHandler invocation=new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //添加辅助功能
                System.out.println("=====辅助功能=======发布租房信息=====");
                System.out.println("=====辅助功能=======中介带租客看房=====");

                //调用核心功能
                fangDongService.zuFang();

                //添加辅助功能
                System.out.println("======辅助功能=======中介替房东修防止===========");
                System.out.println("=======辅助功能====中介替房东承担法律风险========");
                return null;
            }
        };

        //动态生成代理实例对象并调用
        //第一参数: 当前类的类加载器,
        //第二参数: 接口的字节码
        //第三参数: InvocationHandler对象, 对象里面添加了额外功能
        FangDongService proxy= (FangDongService) Proxy.newProxyInstance(TestDynamicProxy.class.getClassLoader(),
                fangDongService.getClass().getInterfaces(),invocation);

        proxy.zuFang();
    }

    /**
     * CGlib实现动态代理
     */
    @Test
    public void testCGLIB(){
        // 代理目标
        FangDongService fangDongService = new FangDongServiceImpl();

        //创建字节码增强器，用来给类创建代理
        Enhancer enhancer = new Enhancer();
        //指定被代理类, 将被代理类字节码传入
        enhancer.setSuperclass(FangDongServiceImpl.class);
        //实现代理中的额外功能
        enhancer.setCallback(new org.springframework.cglib.proxy.InvocationHandler() {
            @Override
            public Object invoke(Object o, Method method, Object[] objects) throws Throwable {
                // 辅助功能、额外功能
                System.out.println("发布租房信息2");
                System.out.println("带租客看房2");
                // 核心
                fangDongService.zuFang();
                return null;
            }
        });


        // 动态生成代理类
        FangDongServiceImpl proxy = (FangDongServiceImpl)enhancer.create();

        proxy.zuFang();
    }
}
