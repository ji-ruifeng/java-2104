package com.qf.service;

import com.qf.dao.UserDao;
import com.qf.dao.UserDaoTwoImpl;
import com.qf.pojo.User;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/28 10:22
 * description:
 */

public class UserServiceImpl implements UserService{
    @Override
    public List<User> getUserAll() {
        UserDao userDao = new UserDaoTwoImpl();
        List<User> list = userDao.findUserAll();
        return list;
    }
}
