package com.qf.factory;

import java.io.InputStream;
import java.util.Properties;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/28 10:10
 * description:创建javaBean的工厂类
 */

public class BeanFactory {
    private Properties properties=new Properties();

    public BeanFactory() {

    }

    /**
     * 构造方法
     * @param config 配置文件位置以及名字
     */
    public BeanFactory(String config)throws Exception{
        //通过流读取资源属性文件
        InputStream inputStream=BeanFactory.class.getResourceAsStream(config);
        properties.load(inputStream);
    }

    /**
     * 通过javaBean名字获取类的实例化对象
     * @param beanName bean名字
     */
    public Object getBean(String beanName)throws Exception {

        //1. 通过bean的名字到资源文件中获取这个对应的实现类的全限定名
        String classPath = properties.getProperty(beanName);

        //2. 使用反射, 获取实例化对象
        Class clazz = Class.forName(classPath);
        Object obj = clazz.newInstance();
        return obj;
    }
}
