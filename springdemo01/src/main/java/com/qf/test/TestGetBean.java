package com.qf.test;


import com.qf.dao.UserDao;
import com.qf.factory.BeanFactory;
import com.qf.pojo.User;
import com.qf.service.UserService;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/28 10:22
 * description:
 */

public class TestGetBean {
    public static void main(String[] args) throws Exception {
        //1.获取bean工厂
        BeanFactory beanFactory=new BeanFactory("/bean.perproties");
        //2.通过名字获取userdao对象
        UserDao userDao= (UserDao) beanFactory.getBean("userDao");
        List<User> list=userDao.findUserAll();
        System.out.println("======"+list);

        UserService userService = (UserService) beanFactory.getBean("userService");
        List<User> list2 = userService.getUserAll();
        System.out.println("=====" + list2);

    }

}
