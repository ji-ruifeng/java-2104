package com.qf.dao;

import com.qf.pojo.User;

import java.util.ArrayList;
import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/28 10:18
 * description:
 */

public class UserDaoTwoImpl implements UserDao{
    @Override
    public List<User> findUserAll() {
        User user = new User();
        user.setId(1);
        user.setName("青龙");
        user.setSex("性别");

        List<User> list = new ArrayList<>();
        list.add(user);
        return list;
    }
}
