package com.qf.dao;

import com.qf.pojo.User;

import java.util.ArrayList;
import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/28 10:17
 * description:
 */

public class UserDaoImpl implements UserDao{
    @Override
    public List<User> findUserAll() {
        return new ArrayList<User>();
    }
}
