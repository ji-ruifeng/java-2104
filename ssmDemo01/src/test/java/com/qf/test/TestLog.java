package com.qf.test;


import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/2 19:35
 * description:
 */

public class TestLog {
    Logger logger= Logger.getLogger(TestLog.class);

    @Test
    public void test(){
        logger.error("error");
        logger.debug("debug");
        logger.info("info");
        logger.warn("warn");
        logger.fatal("fatal");
        logger.trace("trace");
    }
}
