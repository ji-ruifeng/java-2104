package com.qf.pojo;

import java.util.List;

/**
 * @author zhaojian
 */
public class TestPojo {
    private List<User> userList;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public String toString() {
        return "TestPojo{" +
                "userList=" + userList +
                '}';
    }

}
