package com.qf.mapper;

import com.qf.pojo.User;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/2 19:23
 * description:
 */
public interface UserMapper {
    @Select("select * from t_users")
    public List<User> findUserAll();
}
