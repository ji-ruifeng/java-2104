package com.qf.service;

import com.qf.pojo.User;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/2 19:23
 * description:
 */
public interface UserService {
    public List<User> findAll();
}
