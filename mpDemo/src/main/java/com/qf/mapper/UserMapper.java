package com.qf.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.pojo.User;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/10 11:02
 * description:
 */
public interface UserMapper extends BaseMapper<User> {
}
