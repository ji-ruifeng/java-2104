package com.qf.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import sun.text.normalizer.UBiDiProps;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/10 11:03
 * description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class TestUserMapper {
    @Autowired
    private UserMapper userMapper;

    /**
     * 查询所有
     */
    @Test
    public void testSelectAll(){
        List<User> list=userMapper.selectList(null);
        System.out.println("====="+list);
    }

    /**
     * 根据主键查询
     */
    @Test
    public void testFindBuId(){
        User user=userMapper.selectById(1);
        System.out.println("====="+user);
    }

    /**
     * 根据名字模糊查询，根据年龄范围查询
     */
    @Test
    public void testFindByNameAndAge(){
        //创建查询条件对象
        QueryWrapper<User> queryWrapper=new QueryWrapper<User>();
        //queryWrapper.like("name", "青");
        queryWrapper.ge("age", 17);
        //根据年龄降序排序
        queryWrapper.orderByDesc("age");

        //查询
        List<User> list = userMapper.selectList(queryWrapper);
        System.out.println("=====" + list);
    }

    @Test
    public void testInsert() {
        User user = new User();
        user.setName("穷奇");
        user.setPassword("123456");
        user.setAge(17);
        user.setUserName("taowu");
        user.setEmail("taowu@qq.com");

        int count = userMapper.insert(user);
        System.out.println("========" + count);


    }


    @Test
    public void testPage() {
        /**
         * 创建查询条件对象
         */
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.ge("age", 17);

        /**
         * 设置分页对象
         * 第一参数: 当前页, 第二参数: 每页显示条数
         */
        Page<User> page = new Page<>(2, 2);
        //查询, 第一参数:分页对象, 第二参数: 分页查询条件
        IPage<User> iPage = userMapper.selectPage(page, queryWrapper);

        //查询到的结果集
        List<User> list = iPage.getRecords();
        long total = iPage.getTotal();
        long size = iPage.getSize();
        System.out.println("======" + list);
    }



}
