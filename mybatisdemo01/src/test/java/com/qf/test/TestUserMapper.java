package com.qf.test;

import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/26 16:39
 * description:
 */

public class TestUserMapper {
    /**
     * SqlSessionFactory是线程安全的所以可以全局使用
     */
    private SqlSessionFactory sqlSessionFactory;

    /**
     * 初始化会话工厂
     * @bedore注解的用意是使这个方法在执行junit测试方法钱执行
     */
    @Before
    public void init() throws Exception{
        //1.通过输出流来读取mybatis核心配置文件mybatis-config.xml
        InputStream inputStream= Resources.getResourceAsStream("mybatis-config.xml");
        //2.根据读取到核心配置文件中的内容创建会话工厂
        sqlSessionFactory=new SqlSessionFactoryBuilder().build(inputStream);

    }
    @Test
    public void TestFindAll()throws Exception{
        //3.根据会话工厂创建会话对象，SqlSession是线程不安全的，所以必须在方法内使用
        SqlSession sqlSession=sqlSessionFactory.openSession();
        //4.根据会话对象创建Mapper映射接口对象
        UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
        //5.调用Mapper对象中的方法执行
        List<User> list=userMapper.findAll();
        //6打印结果
        if(list!=null) {
            for (User user : list) {
                System.out.println("======" + user);
            }
        }
    }
    @Test
    public void TestFindOne() throws Exception {
        //3. 根据会话工厂创建会话对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4. 根据会话对象创建Mapper映射接口对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        //5. 执行方法
        User user = userMapper.findOne(2);

        //6. 打印返回值
        System.out.println("========" + user);
    }


    @Test
    public void insertUser() {
        //3. 根据会话工厂创建会话对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4. 根据会话对象创建Mapper映射接口对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        User user = new User();
        user.setName("饕鬄");
        user.setSex("男");
        user.setBirthday(new Date());
        user.setRegistTime(new Date());
        user.setPassword("456");

        int count = userMapper.insertUser(user);
        System.out.println("=========" + count);
        System.out.println("=========" + user);

        //提交事务
        sqlSession.commit();
    }

    @Test
    public void updateUser() {
        //3. 根据会话工厂创建会话对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4. 根据会话对象创建Mapper映射接口对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        User user = new User();
        user.setId(9);
        user.setName("梼杌");
        user.setSex("女");
        user.setBirthday(new Date());
        user.setRegistTime(new Date());
        user.setPassword("456");

        int count = userMapper.updateUser(user);
        System.out.println("=========" + count);

        //提交事务
        sqlSession.commit();
    }


    @Test
    public void TestDeleteById() {

        //3. 根据会话工厂创建会话对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4. 根据会话对象创建Mapper映射接口对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        int count = userMapper.deleteById(4);
        System.out.println("========" + count);

        //提交事务
        sqlSession.commit();
    }


    @Test
    public void TestFindUserByNameLike() throws Exception {
        //3. 根据会话工厂创建会话对象, SqlSession是线程不安全的, 所以必须在方法内使用
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4. 根据会话对象创建Mapper映射接口对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        //5. 调用Mapper对象中的方法执行
        List<User> list = userMapper.findUserByNameLike("青");

        //6. 打印结果
        if (list != null) {
            for (User user : list) {
                System.out.println("======" + user);
            }

        }

    }


}
