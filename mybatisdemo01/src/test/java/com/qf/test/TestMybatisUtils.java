package com.qf.test;

import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import com.qf.utils.MyBatisUtils;
import org.junit.Test;

import java.util.Date;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/26 16:38
 * description:
 */

public class TestMybatisUtils {
    @Test
    public void TestInsert() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        User user = new User();
        user.setName("穷奇");
        user.setSex("男");
        user.setBirthday(new Date());
        user.setRegistTime(new Date());
        user.setPassword("456");
        int count = userMapper.insertUser(user);
        System.out.println("=========" + count);

        //提交事务
        MyBatisUtils.commit();

    }
}
