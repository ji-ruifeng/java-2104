package com.qf.test;

import com.qf.mapper.OrderMapper;
import com.qf.pojo.Order;
import com.qf.utils.MyBatisUtils;
import org.junit.Test;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/26 20:50
 * description:
 */

public class TestOrderMapper {
    @Test
    public void testInsertOrder() {
        OrderMapper userMapper = MyBatisUtils.getMapper(OrderMapper.class);

        Order order = new Order();
        order.setName("卫生纸");
        int count = userMapper.insertOrder(order);
        System.out.println("========" + count);
        System.out.println("=========" + order);

        //提交事务
        MyBatisUtils.commit();
    }
}
