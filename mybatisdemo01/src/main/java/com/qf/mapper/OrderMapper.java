package com.qf.mapper;

import com.qf.pojo.Order;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/26 20:49
 * description:
 */
public interface OrderMapper {
    /**
     * 插入商品
     * @param order
     * @return
     */
    public int insertOrder(Order order);
}
