package com.qf.mapper;

import com.qf.pojo.User;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/26 15:20
 * description:
 */
public interface UserMapper {
    /**
     * 查询所有
     * @return
     */
    public List<User> findAll();

    /**
     * 根据主键id查询
     * @param id
     * @return
     */
    public User findOne(Integer id);
    /**
     * 添加
     * @param user
     * @return
     */
    public int insertUser(User user);

    /**
     * 修改
     * @param user
     * @return
     */
    public int updateUser(User user);

    /**
     * 删除
     * @param id
     * @return
     */
    public int deleteById(Integer id);


    List<User> findUserByNameLike(String 青);
}
