package com.qf.pojo;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/26 20:49
 * description:
 */

public class Order {
    private String id;
    private String name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

}
