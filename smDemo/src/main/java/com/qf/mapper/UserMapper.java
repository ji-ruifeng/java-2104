package com.qf.mapper;

import com.qf.pojo.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 14:58
 * description:
 */
public interface UserMapper {
    @Select("select * from t_users")
    public List<User> findAll();

    @Insert("insert into t_users (name, PASSWORD, sex, birthday, registtime) " +
            "values(#{name}, #{password}, #{sex}, #{birthday}, #{registTime})")
    public int insertUser(User user);
}
