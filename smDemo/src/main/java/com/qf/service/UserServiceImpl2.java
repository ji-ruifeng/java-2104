package com.qf.service;

import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/31 17:08
 * description:
 */
@Service("u2")
public class UserServiceImpl2 implements UserService{

    @Autowired
    private UserMapper userMapper;
    @Override
    public List<User> findUserAll() {
        List<User> list=userMapper.findAll();
        return list;
    }

    @Override
    public void insertUser(User user) {

    }
}
