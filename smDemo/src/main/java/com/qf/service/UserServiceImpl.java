package com.qf.service;

import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 14:58
 * description:
 */
//将当前类声明成一个spring中的javabean, 用在service实现类上
@Service("u1")
public class UserServiceImpl implements UserService{
    //注入属性
    @Autowired
    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public List<User> findUserAll() {
        List<User> list=userMapper.findAll();
        return list;
    }

    //使用注解式事务，声明式事务
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void insertUser(User user) {
        userMapper.insertUser(user);
        int i=1/0;
        userMapper.insertUser(user);
    }


}
