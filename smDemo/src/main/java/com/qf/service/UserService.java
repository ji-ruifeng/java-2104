package com.qf.service;

import com.qf.pojo.User;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 14:58
 * description:
 */
public interface UserService {
    public List<User> findUserAll();
    public void insertUser(User user);
}
