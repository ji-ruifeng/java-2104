package com.qf.test;

import com.qf.pojo.User;
import com.qf.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/30 15:03
 * description:
 */
@RunWith(SpringJUnit4ClassRunner.class)//由SpringJUnit4ClassRunner启动测试
@ContextConfiguration("classpath:applicationContext.xml")//spring的配置文件位置
public class TestUserService {
    @Resource(name= "u2")
    private UserService userService;

    //从配合文件中读取属性值进行注入，根据ley来取值
    @Value("${jdbc.url}")
    private String url;

    @Value("${jdbc.username}")
    private String username;

    @Value("${jdbc.password}")
    private String pwd;

    @Test
    public void testFindUserAll(){
        //ApplicationContext applicationContext=new ClassPathXmlApplicationContext("/applicationContext.xml");
        //UserService userService= (UserService) applicationContext.getBean("userService");
        List<User> list=userService.findUserAll();
        System.out.println("========" + url);
        System.out.println("========" + username);
        System.out.println("========" + pwd);
        System.out.println("======"+list);
    }

    @Test
    public void testInsertUser(){
        User user=new User();
        user.setName("饕鬄");
        user.setSex("男");
        user.setPassword("123");
        user.setBirthday(new Date());
        user.setRegistTime(new Date());
        userService.insertUser(user);


    }

}
