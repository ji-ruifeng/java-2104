package com.qf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/2 21:01
 * description:
 */
@Controller
@RequestMapping("/crossTest")
public class TestController {
    /**
     * 跳转到演示界面
     */
    @RequestMapping("/cross")
    public String test1(){
        return "crossDemo";
    }
}
