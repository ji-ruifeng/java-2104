package com.qf.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/24 19:28
 * description:
 */
@WebServlet("/test")
public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        System.out.println("=========访问到了这个servlet=========");
    }
    @Override
    protected void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException{
        doGet(req,resp);
    }
}
