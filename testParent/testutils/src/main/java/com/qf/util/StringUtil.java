package com.qf.util;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/24 19:18
 * description:
 */

public class StringUtil {
    public static boolean isEmpty(String str) {
        if (str == null) {
            return true;
        }
        if("".equals(str)){
            return true;
        }
        if("".equals(str.trim())){
            return true;
        }
        return false;
    }

}
