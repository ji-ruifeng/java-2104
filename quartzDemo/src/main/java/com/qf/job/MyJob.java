package com.qf.job;

import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/10 19:34
 * description:自定义执行的任务, 也就是需要执行的业务
 */

public class MyJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        //创建工作详情对象
        JobDetail jobDetail = context.getJobDetail();
        //读取工作的名称
        String name = jobDetail.getKey().getName();
        //读取工作所在的组
        String group = jobDetail.getKey().getGroup();

        System.out.println("====任务执行,==jobName=="+name+"==jobGroup==" + group);
    }

}
