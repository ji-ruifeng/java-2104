package com.qf.job;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/10 19:34
 * description:
 */

public class TestMyJob  {
    public static void main(String[] args) throws Exception {
        //1. 创建调度器Scheduler
        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        //2. 创建触发器,
        CronTrigger trigger = TriggerBuilder
                .newTrigger()
                //第一个参数为任务名称, 第二个参数为组名
                .withIdentity("name1", "group1")
                //设置触发执行的时间, 使用cron表达式
                .withSchedule(CronScheduleBuilder.cronSchedule("* * * * * ?"))
                .build();

        //3. 任务详情JobDetail对象, 具体要执行的内容对象
        JobDetail jobDetail = JobBuilder.newJob(MyJob.class).withIdentity("job1", "group1").build();

        //4. 将任务详情JobDeatail和触发器添加到调度器中
        scheduler.scheduleJob(jobDetail, trigger);

        //5. 启动, 调度器开始执行
        scheduler.start();
    }
}
