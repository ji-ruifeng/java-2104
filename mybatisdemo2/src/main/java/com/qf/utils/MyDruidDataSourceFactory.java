package com.qf.utils;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.datasource.pooled.PooledDataSourceFactory;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/27 16:21
 * description:
 */

public class MyDruidDataSourceFactory extends PooledDataSourceFactory {
    public MyDruidDataSourceFactory(){
        this.dataSource=new DruidDataSource();
        //替换数据源
    }
}
