package com.qf.mapper;

import com.qf.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/27 16:20
 * description:
 */
public interface UserMapper {
    /**
     * 查询所有用户和用户详情
     * 演示一对一
     * @return
     */
    public List<User> findUserAndDesc();

    /**
     * 查询所有用户，订单以及商品
     * 演示多对多
     * @return
     */
    public List<User> findUserAndOrderAndProduct();

    /**
     * 查询所有用户和订单
     * 演示一对多
     * @return
     */
    public List<User> findUserAndOrder();

    /**
     * 查询用户表所有数据
     * @return
     */
    @Select("select * from t_users")
    public List<User> findUserAll();

    /**
     * 根据名字与姓名动态拼接条件查询
     * @return
     */
    public List<User> findUserByNameAndSex(@Param("name") String name,@Param("sex") String sex);

    /**
     * 更新用户表
     * @param user
     */
    public int updateUser(User user);

    /**
     * 根据id集合查询用户
     * @param ids
     * @return
     */
    public List<User> findUserByIds(List<Integer> ids);
}
