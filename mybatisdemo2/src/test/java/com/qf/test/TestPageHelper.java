package com.qf.test;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import com.qf.utils.MyBatisUtils;
import org.junit.Test;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/27 16:25
 * description:
 */

public class TestPageHelper {
    @Test
    public void testPageHelper() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        //设置分页插件从第几页开始查询, 每页查询多少条数据
        //第一个参数: 从第几页开始查询, 第二个参数: 每页查询条数
        PageHelper.startPage(1, 3);
        //执行sql语句查询
        List<User> list = userMapper.findUserAll();

        //分装分页对象
        PageInfo<User> pageInfo = new PageInfo<>(list);

        System.out.println("====查询到的本页集合数据=====" + pageInfo.getList());
        System.out.println("====总条数=====" + pageInfo.getTotal());
        System.out.println("====总页数=====" + pageInfo.getPages());




    }
}
