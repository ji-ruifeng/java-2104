package com.qf.test;

import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import com.qf.utils.MyBatisUtils;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/27 16:25
 * description:
 */

public class TestCache {
    /**
     * 测试一级缓存
     */
    @Test
    public void testOneCache() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        //第一次查询
        List<User> list1 = userMapper.findUserAll();
        System.out.println("==========" + list1);

        //第二次查询
        List<User> list2 = userMapper.findUserAll();
        System.out.println("======" + list2);
    }


    /**
     * 测试二级缓存
     */
    @Test
    public void testSecondCache() throws Exception{
        //1. 通过输入流来读取mybatis核心配置文件mybatis-config.xml
        InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");

        //2. 根据读取到核心配置文件中的内容创建会话工厂
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //3. 开启会话对象
        SqlSession sqlSession = sqlSessionFactory.openSession();

        //4. 通过会话对象获取UserMapper实例化对象
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);

        //第一次查询
        List<User> list1 = userMapper.findUserAll();
        System.out.println("=========" + list1);
        //关闭sqlSession会话
        sqlSession.close();


        //5. 再次开启一个新的sqlSession会话对象
        SqlSession sqlSession2 = sqlSessionFactory.openSession();
        UserMapper userMapper2 = sqlSession2.getMapper(UserMapper.class);
        List<User> list2 = userMapper2.findUserAll();
        System.out.println("========" + list2);
        sqlSession2.close();

    }

}


