package com.qf.test;

import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import com.qf.utils.MyBatisUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/27 16:25
 * description:
 */

public class TestUserMapper {
    /**
     * 演示一对一
     */
    @Test
    public void testFindUserAndDesc() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        List<User> list = userMapper.findUserAndDesc();
        System.out.println("========"+ list);
    }


    /**
     * 演示一对多
     */
    @Test
    public void testFindUserAndOrder() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        List<User> list = userMapper.findUserAndOrder();
        System.out.println("======" + list);
    }


    /**
     * 演示多对多
     */
    @Test
    public void testFindUserAndOrderAndProdcut() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        List<User> list = userMapper.findUserAndOrderAndProduct();
        System.out.println("======" + list);
    }

    @Test
    public void testFindUserAll() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        List<User> list = userMapper.findUserAll();
        System.out.println("=====" + list);
    }


    @Test
    public void testFindUserByNameAndSex() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        User user = new User();
        //user.setName("张");
        user.setSex("男");
        List<User> list = userMapper.findUserByNameAndSex("张", "男");
        System.out.println("========" + list);
    }


    @Test
    public void testUpdateUser() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        User user = new User();
        user.setName("穷奇");
        user.setSex("男");
        user.setId(8);
        int count = userMapper.updateUser(user);

        MyBatisUtils.commit();
        System.out.println("========" + count);
    }


    @Test
    public void testFindUserByIds() {
        UserMapper userMapper = MyBatisUtils.getMapper(UserMapper.class);

        List<Integer> ids = new ArrayList<>();
        ids.add(1);
        ids.add(3);
        ids.add(6);
        ids.add(8);

        List<User> list = userMapper.findUserByIds(ids);
        System.out.println("========" + list);
    }

}
