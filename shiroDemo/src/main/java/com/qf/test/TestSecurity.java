package com.qf.test;

import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.crypto.hash.Sha1Hash;
import org.apache.shiro.crypto.hash.Sha256Hash;

import java.util.UUID;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/6 19:52
 * description:
 */

public class TestSecurity {
    public static void main(String[] args) {
        //盐值
        String salt= UUID.randomUUID().toString();
        //md5加密
        Md5Hash md5Hash=new Md5Hash("admin",salt,1000);
        System.out.println(md5Hash.toString());
        //sha1加密
        Sha1Hash sha1Hash = new Sha1Hash("admin",salt,1024);
        System.out.println(sha1Hash.toString());
        //sha256加密
        Sha256Hash sha256Hash = new Sha256Hash("admin",salt,1000);
        System.out.println(sha256Hash.toString());
        //加密张三用户, md5加密
        Md5Hash zhangsan = new Md5Hash("123456", "zhangsan", 1024);
        System.out.println(zhangsan.toString());
    }
}
