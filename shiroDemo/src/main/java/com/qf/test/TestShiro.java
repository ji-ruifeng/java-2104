package com.qf.test;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/6 19:52
 * description:
 */

public class TestShiro {
    public static void main(String[] args) {
        //1.创建securityManager权限管理对象，全局一个
        DefaultSecurityManager securityManager=new DefaultSecurityManager();
        //获取shiro.ini配置文件中的用户，角色，权限数据，交给shiro框架
        IniRealm iniRealm=new IniRealm("classpath:shiro.ini");
        //将realm对象放入权限管理对象中
        securityManager.setRealm(iniRealm);
        //2.设置权限工具类SecurityUtils,将权限管理对象放入工具类
        SecurityUtils.setSecurityManager(securityManager);
        //3.获取Subject主题对象，主题对象中有当前用户
        Subject subject=SecurityUtils.getSubject();
        //4.登录，输入用户名密码
        UsernamePasswordToken token =new UsernamePasswordToken("zhangsan","123456");
        //5.调用登录方法，调用登录方法后
        //shiro会自动对比用户输入的用户名密码和配置文件或者表中的用户名密码
        try {
            subject.login(token);
            System.out.println("====登录成功===");
        } catch (UnknownAccountException e) {
            System.out.println("账号不存在");
        }catch (IncorrectCredentialsException e){
            System.out.println("密码错误");
        }catch (AuthenticationException e){
            System.out.println("其他异常");
        }


        System.out.println("===================================");
        System.out.println(subject.isAuthenticated());//判断用户是否登录成功
        System.out.println("-------角色 hasRole()-------");
        System.out.println(subject.hasRole("admin")?"具有admin角色":"不具有admin角色");
        System.out.println(subject.hasRole("developer")?"具有开发者角色":"不具有开发者角色");
        System.out.println(subject.hasRole("saler")?"具有saler角色":"不具有saler角色");
        System.out.println(subject.hasRole("teacher")?"具有teacher角色":"不具有teacher角色");
        System.out.println("-------角色-------");


        System.out.println("-------权限 isPermitted(\"sys:user:*\")-------");
        //admin=sys:user:*,sys:menu:add,shop:goods:*
        System.out.println(subject.isPermitted("sys:user:add")?"具有新增用户的权限":"不具有新增用户的权限");
        System.out.println(subject.isPermitted("sys:user:del")?"具有删除用户的权限":"不具有删除用户的权限");

        System.out.println(subject.isPermitted("sys:menu:add")?"具有新增菜单的权限":"不具有新增菜单的权限");
        System.out.println(subject.isPermitted("sys:menu:del")?"具有删除菜单的权限":"不具有删除菜单的权限");

        System.out.println(subject.isPermitted("shop:order:add")?"具有新增订单的权限":"不具有新增订单的权限");
        System.out.println(subject.isPermitted("shop:order:del")?"具有删除订单的权限":"不具有删除订单的权限");

        System.out.println(subject.isPermitted("shop:goods:add")?"具有新增商品的权限":"不具有新增商品的权限");
        System.out.println(subject.isPermitted("shop:goods:del")?"具有删除商品的权限":"不具有删除商品的权限");


    }

}
