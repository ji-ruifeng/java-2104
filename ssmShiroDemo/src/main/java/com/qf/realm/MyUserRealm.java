package com.qf.realm;


import com.qf.pojo.SysMenu;
import com.qf.pojo.SysRole;
import com.qf.pojo.SysUsers;
import com.qf.service.MenuService;
import com.qf.service.RoleService;
import com.qf.service.UserService;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import sun.java2d.pipe.SpanShapeRenderer;

import java.util.List;
import java.util.Set;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:11
 * description:自定义realm对象
 * 1. 从数据库中根据用户名, 取出数据库中的用户名, 密码交给shiro框架管理
 * 2. 根据用户名, 到数据库取出用户对应的角色和权限对象交给shiro框架管理
 */
public class MyUserRealm extends AuthorizingRealm {

    @Autowired
    private UserService userService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private RoleService roleService;

    /**
     * 用户的用户名, 密码输入正确, 校验完成后进行赋权操作
     * 根据用户名, 到数据库查询这个用户对应的角色和权限, 交给shiro管理
     * 调用时机: 在需要访问资源的时候, 需要角色和权限的时候才会调用此方法
     * @param principal
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
        System.out.println("======已登录成功, 授予登录用户权限, 也就是赋予用户对应的角色和能够访问的菜单========");

        //1. 获取当前登录的用户对象
        SysUsers sysUsers = (SysUsers) principal.getPrimaryPrincipal();

        //2. 获取当前登录用户的, 用户id
        int uid = sysUsers.getId();

        //3. 根据用户id, 查询数据库中这个用户对应的角色集合和权限集合
        Set<String> roleList = roleService.findRoleListByUid(uid);
        Set<String> menuList = menuService.findMenuListByUid(uid);

        //4. 创建shiro中的用户权限对象
        SimpleAuthorizationInfo auth = new SimpleAuthorizationInfo();

        //5. 将查询到的角色集合放入shiro的权限对象
        auth.setRoles(roleList);
        //6. 将查询到的权限集合放入shiro权限对象
        auth.setStringPermissions(menuList);
        //7. 返回shiro权限对象
        return auth;
    }

    /**
     * 根据用户在页面输入的用户名, 查询数据库中的用户名密码, 交给shiro框架
     * 让shiro框架进行对比用户名, 密码是否正确
     * 调用时机: 在controller中调用subject.login(token);方法就会执行这个方法, 进行用户名, 密码校验
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        System.out.println("===MyUserRealm====开始校验用户名密码========");
        //1. 获取用户在浏览器中输入的用户名
        String userName = (String)auth.getPrincipal();

        //2. 根据用户在页面输入的用户名, 查询数据库获取用户对象
        String password = new String((char[])auth.getCredentials());

        //3. 判断用户对象是否为空, 如果为空抛出异常
        SysUsers sysUser = userService.findUserByName(userName);

        if (sysUser == null) {
            throw new UnknownAccountException("账号不存在, 请先注册, 在登录!");
        }

        //4. 对比数据库中的密码和用户输入的密码是否一致
        if (!password.equals(sysUser.getPassword())) {
            throw new IncorrectCredentialsException("密码错误");
        }

        //5. 判断用户状态, 1正常, 其他为锁定状态
        if (sysUser.getStatus() != 1) {
            throw new  LockedAccountException("账号被锁定不允许登录!");
        }


        //6. 封装shiro中需要的权限对象, 包括用户名, 密码, 以及当前用户对象交给shiro返回
        //第一参数:用户名, 第二参数: 密码, 第三参数: 自定义Realm名字, 通过this获取
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(userName, password, this.getName());
        return info;
    }

}

