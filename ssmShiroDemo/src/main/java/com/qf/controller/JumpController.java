package com.qf.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 17:41
 * description:
 */
@Controller
@RequestMapping("/jump")
public class JumpController {
    /**
     * 限制用户是否能访问到一个界面
     */
    @RequestMapping("/index")
    @RequiresPermissions("sys:user:select")
    public String toIndexJsp(){
        return "/WEB-INF/index.jsp";
    }


}
