package com.qf.controller;

import com.qf.utils.Result;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:10
 * description:
 */
@Controller
public class LoginController {

    /**
     * 登录方法
     * @return
     */
    @RequestMapping("/doLogin")
    @ResponseBody
    public Result login(String username, String password, boolean rememberMe) {

        //1. 获取shiro中的subject对象
        Subject subject = SecurityUtils.getSubject();

        //2. 对用户从页面输入的密码进行加密处理
        //第一个参数: 需要加密的明文, 第二个参数: 盐值, 第三:hash算法执行的次数
        password = new Md5Hash(password, username, 1024).toString();
        System.out.println("======加密后的密码:=====" + password);

        //3. 创建shiro中的用户名和密码对象, 将用户输入的用户名密码交给shiro管理
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);

        //4. 关于记住我的设置
        System.out.println("=======记住我====" + rememberMe);
        if (rememberMe) {
            //设置使用cookie记住用户输入的用户名, 密码
            token.setRememberMe(true);
        }

        //5. 调用shiro的登录方法, 调用后, shiro会自动执行Realm实现类
        try {
            subject.login(token);
            System.out.println("======登录开始执行=========");
        } catch (UnknownAccountException e) {
            return new Result(1, "账号不存在");
        }catch (IncorrectCredentialsException e){
            return new Result(1, "密码错误");
        }catch (AuthenticationException e){
            System.out.println("其他异常");
            return new Result(1, "其他异常");
        }

        return new Result(0, "登录成功!");
    }

    /**
     * 登录并跳转到响应页面
     * @param username
     * @param password
     * @param rememberMe
     * @return
     */
    @RequestMapping("/doLogin1")
    public String doLogin1(String username,String password,boolean rememberMe){
        Session session = null;
        try {
            //1,得到subject
            Subject subject = SecurityUtils.getSubject();
            //得到会话
            session =  subject.getSession();

            //对密码加密
            password = new Md5Hash(password,username,1024).toString();
            System.out.println("密码加密后："+password);

            //2,username和password封装成usernamePasswordToken对象
            UsernamePasswordToken upt = new UsernamePasswordToken(username,password);

            //3. 记住我
            System.out.println("rememberMe="+rememberMe);
            if (rememberMe){
                //Cookie
                upt.setRememberMe(true);//记住我' 第一步
            }

            //4. 登录
            subject.login(upt);


            //shiro内部调用自定义Realm（shiro.ini）的认证(doGetAuthenticationInfo)方法
            session.setAttribute("username",username);
            return  "/index.jsp";
        } catch (Exception e) {

            session.setAttribute("error",e.getMessage());
        }

        return  "/login.jsp";

    }

    /**
     * 注销当前登录用户
     * @return
     */
    @RequestMapping("/logout")
    @ResponseBody
    public Result logout() {

        //1. 获取subject对象
        Subject subject = SecurityUtils.getSubject();
        //2. 调用注销方法
        subject.logout();
        //3. 返回
        return Result.ok("注销成功!");
    }
}

