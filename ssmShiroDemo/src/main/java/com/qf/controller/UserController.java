package com.qf.controller;

import com.qf.pojo.SysUsers;
import com.qf.service.UserService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 17:43
 * description:
 */
@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * 默认不设置权限字符串, 是只要登录就可以访问
     * @return
     */
    @RequestMapping("/findAll")
    public List<SysUsers> findUserAll() {
        List<SysUsers> list = userService.findAll();
        return list;
    }

    /**
     * 设置游客访问
     * @return
     */
    @RequestMapping("/findAll2")
    @RequiresGuest
    public List<SysUsers> findUserAll1() {
        List<SysUsers> list = userService.findAll();
        return list;
    }

    /**
     * 限制角色方案
     * @return
     */
    @RequestMapping("/findAll3")
    @RequiresRoles(value = {"管理员"},logical = Logical.OR)
    public List<SysUsers> findUserAll3() {
        List<SysUsers> list = userService.findAll();
        return list;
    }

    /**
     * 限制permission权限字符串
     * @return
     */
    @RequestMapping("/findAll4")
    @RequiresPermissions("sys:user:select")
    public List<SysUsers> findUserAll4() {
        List<SysUsers> list = userService.findAll();
        return list;
    }


}
