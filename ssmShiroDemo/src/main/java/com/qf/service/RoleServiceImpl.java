package com.qf.service;

import com.qf.dao.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:12
 * description:
 */

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Override
    public Set<String> findRoleListByUid(Integer uid) {
        Set<String> list = roleDao.findRoleListByUid(uid);
        return list;
    }

}

