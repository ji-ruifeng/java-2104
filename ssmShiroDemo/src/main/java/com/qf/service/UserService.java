package com.qf.service;

import com.qf.pojo.SysUsers;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:12
 * description:
 */
public interface UserService {
    /**
     * 根据用户名获取用户对象返回
     * @param userName
     * @return
     */
    public SysUsers findUserByName(String userName);

    /**
     * 查询所有用户
     * @return
     */
    public List<SysUsers> findAll();
}
