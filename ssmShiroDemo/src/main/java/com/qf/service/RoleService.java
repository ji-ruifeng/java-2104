package com.qf.service;

import java.util.Set;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:12
 * description:
 */
public interface RoleService {
    /**
     * 根据用户id获取角色集合
     * @param uid
     * @return
     */
    public Set<String> findRoleListByUid(Integer uid);
}
