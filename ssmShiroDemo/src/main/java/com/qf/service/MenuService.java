package com.qf.service;

import java.util.Set;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:11
 * description:
 */
public interface MenuService {
    /**
     * 根据用户获取这个用户的菜单集合(权限集合)
     * @param uid
     * @return
     */
    public Set<String> findMenuListByUid(Integer uid);
}
