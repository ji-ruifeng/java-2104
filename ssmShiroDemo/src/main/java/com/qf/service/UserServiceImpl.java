package com.qf.service;

import com.qf.dao.UserDao;
import com.qf.pojo.SysUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:12
 * description:
 */
@Service
public class UserServiceImpl implements UserService{
    private final UserDao userDao;

    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public SysUsers findUserByName(String userName) {
        List<SysUsers> list=userDao.findUserByName(userName);
        if(list!=null&& list.size()>0){
            return list.get(0);
        }
        return null;
    }

    @Override
    public List<SysUsers> findAll() {
        List<SysUsers> list=userDao.findAll();
        return list;
    }
}
