package com.qf.service;

import com.qf.dao.MenuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:12
 * description:
 */
@Service
public class MenuServiceImpl implements MenuService{
    @Autowired
    private MenuDao menuDao;

    @Override
    public Set<String> findMenuListByUid(Integer uid) {
        Set<String> list = menuDao.findMenuListByUid(uid);
        return list;
    }
}
