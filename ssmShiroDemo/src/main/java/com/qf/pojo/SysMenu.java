package com.qf.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhaojian
 */
@Data
public class SysMenu implements Serializable {

    //菜单id
    private Integer id;

    //菜单名字
    private String name;

    //符合shiro规范的权限字符串
    private String perms;

    //类型
    private Integer type;

    //上级id
    private Integer parentId;
}
