package com.qf.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author zhaojian
 */
@Data
public class SysRole implements Serializable {

    //角色id
    private Integer id;

    //角色名字
    private String name;

    //角色描述
    private String desc;

}
