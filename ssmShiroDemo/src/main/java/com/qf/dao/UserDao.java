package com.qf.dao;

import com.qf.pojo.SysUsers;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:11
 * description:
 */
public interface UserDao {
    /**
     * 根据用户名获取用户集合
     */
    @Select("select * from sys_users where username=#{name}")
    public List<SysUsers> findUserByName(@PathVariable("name") String userName);

    /**
     * 查询所有用户
     */
    @Select("select * from sys_users")
    public List<SysUsers> findAll();
}