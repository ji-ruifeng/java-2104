package com.qf.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Set;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:10
 * description:
 */
public interface MenuDao {

    @Select("select DISTINCT e.perms  from sys_role a " +
            "left join sys_user_role b on a.id = b.r_id " +
            "left join sys_users c on c.id = b.u_id  " +
            "left join sys_role_menu d on d.r_id = a.id " +
            "left join sys_menu e on e.id = d.m_id " +
            "where c.id=#{uid}")
    /**
     * 根据uid查询权限列表
     * @return
     */
    public Set<String> findMenuListByUid(@Param("uid") Integer uid);
}