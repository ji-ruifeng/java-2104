package com.qf.dao;

import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Set;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/7 11:10
 * description:
 */
public interface RoleDao {

    @Select("select a.name from sys_role a " +
            "left join sys_user_role b on a.id = b.r_id " +
            "left join sys_users c on c.id = b.u_id  " +
            "where c.id=#{uid}")
    public Set<String> findRoleListByUid(@PathVariable("uid") Integer uid);
}
