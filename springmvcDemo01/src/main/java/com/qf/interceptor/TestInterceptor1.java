package com.qf.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/1 16:29
 * description:拦截器, 拦截所有请求
 */

public class TestInterceptor1 implements HandlerInterceptor {
    /**
     * 执行时机:
     *          请求进入springMvc中, 没有执行到controller方法的时候执行这个方法
     * 使用场景: 做权限校验, 判断当前请求是否有权限访问这个项目
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,Object handler) throws Exception{
        System.out.println("=====TestInterceptor1=========preHandle方法被执行========");

        //返回true放行, 返回false拦截
        return true;
    }

    /**
     * 执行时机:
     *      当请求执行完controler方法后, 但是controller方法的返回值还没有返回给用户的时候, 执行这个方法
     * 使用场景:
     *      可以进行整个项目的全局数据处理, 给整个项目的所有controller方法添加都需要使用的数据
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {

        System.out.println("=======TestInterceptor1==========postHandle方法被执行=======");
    }

    /**
     * 执行时机:
     *      当数据都处理完成, 在返回给页面前执行
     * 使用场景:
     *      记录日志
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("======TestInterceptor1======afterCompletion方法被执行=======");
    }





}
