package com.qf.exception;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.handler.Handler;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/1 16:30
 * description:自定义异常处理器, 所有系统中的异常都会经过这里处理
 */

public class ExceptionResover implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        ModelAndView modelAndView=new ModelAndView();
        //1.判断异常是bug还是我们自己手动抛出的业务异常
        if(e instanceof CustomerException){
            //2.程序员手动抛出的业务异常，本质不算异常，就是消息提示
            modelAndView.addObject("error2","死不认错！");
            modelAndView.setViewName("error2");
        }else{
            //3. 真正的bug, 跳转到错误提示页面, 求原谅
            modelAndView.addObject("error1", "我真的知道错错了, 跪求原谅!");
            modelAndView.setViewName("error1");
        }
        return modelAndView;
    }
}
