package com.qf.exception;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/1 16:30
 * description:自定义异常类
 */

public class CustomerException extends RuntimeException{
    /**
     * 手动抛出的异常消息
     */
    private String msg;

    /**
     * 错误代码
     */
    private String errorCode;

    public CustomerException() {
    }

    public CustomerException(String msg, String errorCode) {
        this.msg = msg;
        this.errorCode = errorCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }




}
