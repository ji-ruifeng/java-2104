package com.qf.controller;

import com.qf.exception.CustomerException;
import com.qf.pojo.TestPojo;
import com.qf.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/8/31 19:44
 * description:  @RequestMapping注解表示从url到controller类的映射关系
 */
@Controller
@RequestMapping("/user")
@SessionAttributes({"userName","city"})
public class UserController {

    //http://localhost:8080/user/findUser
//    @RequestMapping("/findUser")
//    public ModelAndView findUser() {
//        User user = new User();
//        user.setName("张三");
//        user.setSex("男");
//        user.setId(1);
//
//        /**
//         * 模型和视图
//         * model模型中放返回给页面的数据
//         * view视图中放页面的位置以及页面的名字
//         */
//        ModelAndView modelAndView = new ModelAndView();
//        //向模型中放返回给页面的数据
//        modelAndView.addObject("user", user);
//
//        //指定页面位置和名称
//        modelAndView.setViewName("user");
//        return modelAndView;
//    }


    @RequestMapping("/findUser")
    public String findUser(Model model){
        User user = new User();
        user.setName("张三");
        user.setSex("男");
        user.setId(1);

        //演示出异常, 整个演示bug
        //int i = 1/0;

        if (true) {
            CustomerException customerException = new CustomerException("您账户余额不足!", "100100");
            throw customerException;
        }

        //将返回给页面的数据放入model模型中返回给页面
        model.addAttribute("user",user);
        model.addAttribute("userName","session中的名字");
        model.addAttribute("city","session中的城市");

        //默认走视图解析器, 拼接完整页面路径和名字,
        //controller方法返回string是指返回页面路径和名字
        return "userList";
    }

    @RequestMapping("/test1")
    public String test1(HttpServletRequest request,Model model)throws Exception{
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");
//        String name = new String(request.getParameter("name").getBytes(), "utf-8");
//        String sex =  new String(request.getParameter("sex").getBytes(), "utf-8");

        System.out.println("==========" + id + "======" +name +"===="+ sex);
        model.addAttribute("id", id);
        model.addAttribute("name", name);
        /**
         * 演示请求转发:
         * spirngmvc的controller方法返回值是字符串,
         * 返回的字符串中以forward:开头就是请求转发, 后面跟转发的url地址
         */
//        return "forward:/user/test2";

        /**
         * 演示重定向
         * springMvc的controller方法返回值是字符串,
         * 返回的字符串中以redirect:开头就是重定向, 后面跟重定向的url地址
         */
        return "redirect:/user/test2";
    }
    /**
     * 接收基本类型参数
     * @param id
     * @param name
     * @param sex
     * @return
     */
    @RequestMapping("/test2")
    public String test2(Integer id, String name, String sex) {
        System.out.println("==========" + id + "======" +name +"===="+ sex);
        return "userList";
    }

    /**
     * 演示接收实体类类型
     * @param user
     * @return
     */
    @RequestMapping("/test3")
    public String test3(User user) {
        System.out.println("==========" + user.getId() + "======" + user.getName() +"===="+ user.getSex());
        return "userList";
    }

    /**
     * 演示接收数组类型数据
     * @return
     */
    @RequestMapping("/test4")
    public String test4(String[] hobby) {
        System.out.println("==========" + hobby);
        return "userList";
    }

    /**
     * 演示接收List类型
     * @return
     */
    @RequestMapping("/test5")
    public String tset5(TestPojo xxx) {
        if (xxx != null) {
            for (User user : xxx.getUserList()) {
                System.out.println("=========" + user);
            }
        }
        return "userList";
    }

    /**
     * restful类型请求传参
     * @param id
     * @param name
     * @return
     */
    @GetMapping("/test6/{xxx}/{name}")
    public String test6(@PathVariable("xxx") Integer id, @PathVariable("name") String name){
        System.out.println(id + "=======" + name);
        return "userList";
    }




}
