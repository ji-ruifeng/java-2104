package com.qf.controller;

import com.qf.pojo.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/2 16:03
 * description:解决跨域访问问题，允许来自于8081服务器的请求
 */
@RestController
@RequestMapping("/origin")
@CrossOrigin("http://localhost:8081")
public class CrossController {
    /**
     * 跨域访问演示方法
     */
    @RequestMapping("/test1")
    public User test1(){
        User user=new User();
        user.setId(1);
        user.setName("演示跨域");
        user.setSex("男");
        return user;
    }
}
