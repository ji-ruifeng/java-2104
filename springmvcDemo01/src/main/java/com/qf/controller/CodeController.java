package com.qf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/2 16:02
 * description:接收验证码并校验
 */
@Controller
@RequestMapping("/code")
public class CodeController {
    /**
     * 跳转到验证码界面
     */
    @RequestMapping("/code")
    public String toCode(){
        return "code";
    }

    /**
     * 校验验证码
     */
    @RequestMapping("test1")
    public String test1(String captcha, HttpSession session){
        //从会话中获取验证码内容
        String code=String.valueOf(session.getAttribute("captcha"));

        //对比用户输入的验证码和会话中的验证码
        if(code.equalsIgnoreCase(captcha)){
            System.out.println("=====验证码正确=====");
        }else {
            System.out.println("=====验证码错误=====");
        }
        return "code";


    }



}
