package com.qf.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhaojian
 */
public class Order implements Serializable {

    private String id;
    private String name;
    private Integer uid;
    private Integer pid;

    //一个订单对应多个商品
    private List<Product> productList;

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", uid=" + uid +
                ", pid=" + pid +
                ", productList=" + productList +
                '}';
    }

}
