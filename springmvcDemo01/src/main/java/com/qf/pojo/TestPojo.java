package com.qf.pojo;

import java.util.List;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/1 16:14
 * description:
 */

public class TestPojo {
    private List<User> userList;

    public List<User> getUserList(){
        return userList;
    }

    @Override
    public String toString(){
        return "TestPojo{"+
                "userList=" +userList+
                '}';
    }


}
