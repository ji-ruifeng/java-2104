package com.qf.config;

import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.impl.DefaultKaptcha;

import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * projectName: workspace11
 *
 * @author: Arthur
 * time: 2021/9/2 16:04
 * description:
 * 验证码初始化类, 初始化类, 是tomcat启动后会加载项目的web.xml配置文件
 * 并且会加载@configration注解标注的类
 */
@Configuration
public class KaptchaConfig {
    //@Bean注解相当于spring核心配置文件中的<bean>标签, 将这个方法注册成一个javaBean
    @Bean
    public Producer producer(){
        DefaultKaptcha producer=new DefaultKaptcha();
        Properties properties=new Properties ();
        properties.setProperty(Constants.KAPTCHA_BORDER,"no");
        //验证码数量
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_CHAR_LENGTH,"1");
        properties.setProperty(Constants.KAPTCHA_TEXTPRODUCER_FONT_COLOR,"black");
        Config config=new Config(properties);
        producer.setConfig(config);

        return  producer;
    }
}




