<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Title</title>
</head>
<body>
<h2>Hello World!</h2>
<table>
    <tr>
        <td>${user.id}</td>
        <td>${user.name}</td>
        <td>${user.sex}</td>
        <td>${userName}</td>
        <td>${city}</td>
    </tr>
</table>
<form action="/user/test5.action" method="post">
    <%--
    name属性编写规则:
    controller方法中的  : 集合变量名[索引号].实体属性名
    --%>
    id:<input name="userList[0].id" type="text"/>
    名字:<input name="userList[0].name" type="text"/>
    性别: <input name="userList[0].sex" type="text"/>

    </br>
    id:<input name="userList[1].id" type="text"/>
    名字:<input name="userList[1].name" type="text"/>
    性别: <input name="userList[1].sex" type="text"/>

    </br>
    id:<input name="userList[2].id" type="text"/>
    名字:<input name="userList[2].name" type="text"/>
    性别: <input name="userList[2].sex" type="text"/>

    </br>
    id:<input name="userList[3].id" type="text"/>
    名字:<input name="userList[3].name" type="text"/>
    性别: <input name="userList[3].sex" type="text"/>
    <%--爱好:--%>
    <%--LOL<input name="hobby" type="checkbox" value="1">--%>
    <%--刺激<input name="hobby" type="checkbox" value="2">--%>
    <%--农药<input name="hobby" type="checkbox" value="3">--%>
    <%--飞车<input name="hobby" type="checkbox" value="4">--%>
    <input type="submit" value="提交"/>
</form>
</body>
</html>
