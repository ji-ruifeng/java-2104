<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Title</title>
    <script type="application/javascript" src="${pageContext.request.contextPath}/js/jquery-1.4.4.min.js"></script>
    <script type="application/javascript">
        //ajax
        var user = {uid:1,name:"shine"};
        $.ajax({
            url:'${pageContext.request.contextPath}/json/test1',
            type:'post',
            contentType:"application/json",//声明请求参数类型为 json
            data:JSON.stringify(user),// 转换js对象成json
            success:function(ret){
                console.log(ret);
            }
        });
    </script>
</head>
<body>
    欢迎来到千锋, 学习著名的SpringMvc框架<br>
    演示页面传递json数据到controller, 然后controller返回json数据页面解析
</body>
</html>
